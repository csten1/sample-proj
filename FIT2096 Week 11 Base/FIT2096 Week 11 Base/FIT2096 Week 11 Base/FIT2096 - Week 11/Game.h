/*	FIT2096 - Example Code
*	Game.h
*	Created by Elliott Wilson & Mike Yeates - 2016 - Monash University
*	This class is the heart of our game and is also where our game logic will reside
*	It contains the overall Update and Render method for the whole game
*	You should eventually split some game logic into other objects which Game will manage
*	Watch the size of this class - it can grow out of control very fast!
*/

#ifndef GAME_H
#define GAME_H

#include "Direct3D.h"
#include "Camera.h"
#include "InputController.h"
#include "MeshManager.h"
#include "TextureManager.h"
#include "CollisionManager.h"
#include "AudioSystem.h"
#include "SceneLighting.h"
#include "StateMachine.h"
#include "Button.h"
#include "GameObject.h"
#include "Kart.h"
#include "ItemBox.h"

#include "DirectXTK/SpriteBatch.h"
#include "DirectXTK/SpriteFont.h"

#include <vector>

class Game
{
private:
	// All the states our game can be in. The STATE_COUNT down the bottom is just an easy way
	// for us to count how many states this enum has (cast this to an int)
	enum class GameStates
	{
		MENU_STATE,
		GAMEPLAY_STATE,
		PAUSE_STATE,
		STATE_COUNT
	};

	Direct3D* m_renderer;
	AudioSystem* m_audio;
	InputController* m_input;
	MeshManager* m_meshManager;
	TextureManager* m_textureManager;
	CollisionManager* m_collisionManager;
	SceneLighting* m_sceneLighting;
	Camera* m_currentCam;

	// Our state machine is a generic class (we need to tell it what types it manages).
	// It knows about two things - our states, and also who ownes those states.
	StateMachine<GameStates, Game>* m_stateMachine;

	// Sprites / Fonts
	SpriteBatch* m_spriteBatch;
	SpriteFont* m_arialFont12;
	SpriteFont* m_arialFont18;
	Texture* m_currentItemSprite;

	// Our menu screen will have a "start" and "quit" button
	Button* m_startButton;
	Button* m_quitButton;

	// Shaders
	Shader* m_unlitVertexColouredShader;
	Shader* m_unlitTexturedShader;
	Shader* m_diffuseTexturedShader;
	Shader* m_diffuseTexturedFogShader;
	
	Kart* m_player;

	// This contains everything for easy calls to update and render
	std::vector<GameObject*> m_gameObjects;

	// We also need more specific collections for easier collision checks
	std::vector<Kart*> m_karts;
	std::vector<ItemBox*> m_itemBoxes;

	// Initialisation Helpers
	void InitLighting();
	bool InitShaders();
	bool LoadMeshes();
	bool LoadTextures();
	bool LoadAudio();
	void LoadFonts();
	void InitGameWorld();
	void InitStates();
	void InitItemBoxes();

	// UI drawing helpers
	void InitUI();
	void DrawMenuUI();
	void DrawGameUI();
	void DrawPauseUI();
	void BeginUI();
	void EndUI();

	// Every state in our game will have four callbacks
	// We register these with the StateMachine and it calls them for us
	void Menu_OnEnter();
	void Menu_OnUpdate(float timestep);
	void Menu_OnRender();
	void Menu_OnExit();

	void Gameplay_OnEnter();
	void Gameplay_OnUpdate(float timestep);
	void Gameplay_OnRender();
	void Gameplay_OnExit();

	void Pause_OnEnter();
	void Pause_OnUpdate(float timestep);
	void Pause_OnRender();
	void Pause_OnExit();

public:
	Game();	
	~Game();

	bool Initialise(Direct3D* renderer, AudioSystem* audio, InputController* input); //The initialise method will load all of the content for the game (meshes, textures, etc.)

	void Update(float timestep);	//The overall Update method for the game. All gameplay logic will be done somewhere within this method
	void Render();					//The overall Render method for the game. Here all of the meshes that need to be drawn will be drawn

	void Shutdown(); //Cleanup everything we initialised
};

#endif