/*	FIT2096 - Example Code
*	Game.cpp
*	Created by Elliott Wilson & Mike Yeates - 2016 - Monash University
*/

#include "Game.h"
#include "TexturedShader.h"
#include "StaticObject.h"
#include "ThirdPersonCamera.h"
#include "FlyingCamera.h"
#include "MathsHelper.h"
#include "DirectXTK/CommonStates.h"

Game::Game()
{
	m_renderer = NULL;
	m_audio = NULL;
	m_currentCam = NULL;
	m_input = NULL;
	m_meshManager = NULL;
	m_textureManager = NULL;
	m_stateMachine = NULL;
	m_sceneLighting = NULL;
	m_diffuseTexturedShader = NULL;
	m_unlitVertexColouredShader = NULL;
	m_spriteBatch = NULL;
	m_arialFont12 = NULL;
	m_arialFont18 = NULL;
	m_startButton = NULL;
	m_quitButton = NULL;
	m_player = NULL;
}

Game::~Game() {}

bool Game::Initialise(Direct3D* renderer, AudioSystem* audio, InputController* input)
{
	m_renderer = renderer;	
	m_audio = audio;
	m_input = input;
	m_meshManager = new MeshManager();
	m_textureManager = new TextureManager();
	InitLighting();
	
	if (!InitShaders())
		return false;

	if (!LoadMeshes())
		return false;

	if (!LoadTextures())
		return false;

	if (!LoadAudio())
		return false;

	LoadFonts();
	InitUI();
	InitGameWorld();
	InitStates();

	m_collisionManager = new CollisionManager(&m_karts, &m_itemBoxes);
	//m_currentCam = new ThirdPersonCamera(m_player, Vector3(0, 10, -25), true, 2.0f);
	m_currentCam = new FlyingCamera(m_input, Vector3(0.0f, 10.0f, -50.0f));

	return true;
}

void Game::InitLighting()
{
	m_sceneLighting = new SceneLighting(Vector3(0.5, -0.5, 0.5), // Light direction
										Color(0.9f, 0.8f, 0.5f, 1.0f), // Light colour
										Color(0.4f, 0.4f, 0.4f, 1.0f), // Ambient colour
										Color(0.8f, 1.0f, 0.9f, 1.0f), // Fog colour
										10.0f, // Fog start distance
										600.0f); // Fog end distance
}

bool Game::InitShaders()
{
	m_unlitVertexColouredShader = new Shader();
	if (!m_unlitVertexColouredShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/UnlitVertexColourPixelShader.ps"))
		return false;

	m_unlitTexturedShader = new TexturedShader();
	if (!m_unlitTexturedShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/UnlitTexturedPixelShader.ps"))
		return false;

	m_diffuseTexturedShader = new TexturedShader(m_sceneLighting);
	if (!m_diffuseTexturedShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/DiffuseTexturedPixelShader.ps"))
		return false;

	m_diffuseTexturedFogShader = new TexturedShader(m_sceneLighting);
	if (!m_diffuseTexturedFogShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/DiffuseTexturedFogPixelShader.ps"))
		return false;

	return true;
}

bool Game::LoadMeshes()
{
	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/kart.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/ground.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/wall.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/rumble_strip.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/item_box.obj"))
		return false;

	return true;
}

bool Game::LoadTextures()
{
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/kart_red.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/grass.jpg"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/rumble_strip.jpg"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/wall.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/item_box.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_star.png"))
		return false;

	if (!m_textureManager->Load(m_renderer, "Assets/Textures/button.png"))
		return false;

	return true;
}

bool Game::LoadAudio()
{
	if (!m_audio->Load("Assets/Sounds/drumloop.wav"))
		return false;

	if (!m_audio->Load("Assets/Sounds/swish.wav"))
		return false;

	return true;
}

void Game::LoadFonts()
{
	// There's a few different size fonts in there, you know
	m_arialFont12 = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-12pt.spritefont");
	m_arialFont18 = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-18pt.spritefont");
}

void Game::InitUI()
{
	m_spriteBatch = new SpriteBatch(m_renderer->GetDeviceContext());
	
	m_currentItemSprite = m_textureManager->GetTexture("Assets/Textures/sprite_star.png");
	Texture* buttonTexture = m_textureManager->GetTexture("Assets/Textures/button.png");

	// Also init any buttons here
	m_startButton = new Button(128, 64, buttonTexture, L"Start Game", Vector2(574, 385), m_spriteBatch, m_arialFont12, m_input, [this]
	{
		// Transition into the Gameplay state (these buttons are only used on the menu screen)
		m_stateMachine->ChangeState(GameStates::GAMEPLAY_STATE);
	});

	m_quitButton = new Button(128, 64, buttonTexture, L"Quit", Vector2(706, 385), m_spriteBatch, m_arialFont12, m_input, [this]
	{
		// Tell windows to send a WM_QUIT message into our message pump
		PostQuitMessage(0);
	});
}

void Game::InitStates()
{
	// Our state machine needs to know its owner (so it only runs the callbacks while its owner exists)
	m_stateMachine = new StateMachine<GameStates, Game>(this, GameStates::MENU_STATE);

	// Let's match some states with with their OnEnter, OnUpdate, OnRender, OnExit callbacks.
	// Have a look in StateMachine.h to see how this works internally.
	m_stateMachine->RegisterState(GameStates::MENU_STATE, &Game::Menu_OnEnter,
		&Game::Menu_OnUpdate, &Game::Menu_OnRender, &Game::Menu_OnExit);

	m_stateMachine->RegisterState(GameStates::GAMEPLAY_STATE, &Game::Gameplay_OnEnter,
		&Game::Gameplay_OnUpdate, &Game::Gameplay_OnRender, &Game::Gameplay_OnExit);

	m_stateMachine->RegisterState(GameStates::PAUSE_STATE, &Game::Pause_OnEnter,
		&Game::Pause_OnUpdate, &Game::Pause_OnRender, &Game::Pause_OnExit);
}

void Game::InitGameWorld()
{
	InitItemBoxes();

	m_player = new Kart(m_meshManager->GetMesh("Assets/Meshes/kart.obj"),
		m_diffuseTexturedFogShader,
		m_textureManager->GetTexture("Assets/Textures/kart_red.png"),
		Vector3(0, 0, -10),
		m_input);

	m_gameObjects.push_back(m_player);
	m_karts.push_back(m_player);

	// Static scenery objects
	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/ground.obj"),
		m_diffuseTexturedFogShader,
		m_textureManager->GetTexture("Assets/Textures/grass.jpg")));

	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/wall.obj"),
		m_diffuseTexturedFogShader,
		m_textureManager->GetTexture("Assets/Textures/wall.png")));

	m_gameObjects.push_back(new StaticObject(m_meshManager->GetMesh("Assets/Meshes/rumble_strip.obj"),
		m_diffuseTexturedFogShader,
		m_textureManager->GetTexture("Assets/Textures/rumble_strip.jpg")));

}

void Game::InitItemBoxes()
{
	for (int i = 0; i < 20; i++)
	{
		Vector3 position = Vector3(MathsHelper::RandomRange(-200.0f, 200.0f), 0.0f, MathsHelper::RandomRange(-200.0f, 200.0f));

		ItemBox* itemBox = new ItemBox(m_meshManager->GetMesh("Assets/Meshes/item_box.obj"),
			m_diffuseTexturedFogShader,
			m_textureManager->GetTexture("Assets/Textures/item_box.png"),
			position);
		
		m_itemBoxes.push_back(itemBox);
		m_gameObjects.push_back(itemBox);
	}
}

void Game::Update(float timestep)
{
	m_input->BeginUpdate();

	// Assuming audio will be needed across multiple states
	m_audio->Update();

	// The state machine knows what state we're in, so things are nice and simple out here
	m_stateMachine->Update(timestep);

	// If something needs to be done in all states, put it in here.
	// We're only updating the camera in the Gameplay state as that's
	// the only state where it can move.

	m_input->EndUpdate();
}

void Game::Render()
{
	m_renderer->BeginScene(0.8f, 1.0f, 0.9f, 1.0f);

	// Render whatever state we're in
	m_stateMachine->Render();

	m_renderer->EndScene();		
}

void Game::Menu_OnEnter()
{
	OutputDebugString("Menu OnEnter\n");
}

void Game::Menu_OnUpdate(float timestep)
{
	// Button's need to update so they can check if the mouse is over them (they swap to a hover section of the image)
	m_startButton->Update();
	m_quitButton->Update();
}

void Game::Menu_OnRender()
{
	DrawMenuUI();
}

void Game::Menu_OnExit()
{
	OutputDebugString("Menu OnExit\n");
}

void Game::Gameplay_OnEnter()
{
	OutputDebugString("GamePlay OnEnter\n");
}

void Game::Gameplay_OnUpdate(float timestep)
{
	// Update all our gameobjects. What they really are doesn't matter
	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->Update(timestep);
	}

	// Only interested in collisions during the gameplay state
	m_collisionManager->CheckCollisions();

	m_currentCam->Update(timestep);

	// TODO Tell FMOD where the audio listener (camera) is 
	
	// Should we pause
	if (m_input->GetKeyDown('P'))
	{
		m_stateMachine->ChangeState(GameStates::PAUSE_STATE);
	}
}

void Game::Gameplay_OnRender()
{
	// Draw our gameobjects
	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->Render(m_renderer, m_currentCam);
	}

	DrawGameUI();
}

void Game::Gameplay_OnExit()
{
	OutputDebugString("GamePlay OnExit\n");
}

void Game::Pause_OnEnter()
{
	OutputDebugString("Pause OnEnter\n");
}

void Game::Pause_OnUpdate(float timestep)
{
	// Check if we should exit pause
	if (m_input->GetKeyDown('P'))
	{
		m_stateMachine->ChangeState(GameStates::GAMEPLAY_STATE);
	}
}

void Game::Pause_OnRender()
{
	// Keep drawing the game when paused (it's not being updated though which is what freezes it)
	Gameplay_OnRender();

	// In addition to the game, draw some UI to say we're paused
	DrawPauseUI();
}

void Game::Pause_OnExit()
{
	OutputDebugString("Pause OnExit\n");
}

void Game::DrawMenuUI()
{
	BeginUI();

	m_startButton->Render();
	m_quitButton->Render();

	m_arialFont18->DrawString(m_spriteBatch, L"FIT2096 Week 10", Vector2(545, 295), Color(0.0f, 0.0f, 0.0f), 0, Vector2(0, 0));

	EndUI();
}

void Game::DrawGameUI()
{
	BeginUI();

	m_arialFont18->DrawString(m_spriteBatch, L"P to toggle pause", Vector2(540, 680), Color(0.0f, 0.0f, 0.0f), 0, Vector2(0, 0));
	m_spriteBatch->Draw(m_currentItemSprite->GetShaderResourceView(), Vector2(10, 10));

	EndUI();
}

void Game::DrawPauseUI()
{
	BeginUI();
	m_arialFont18->DrawString(m_spriteBatch, L"Paused", Vector2(605, 10), Color(0.0f, 0.0f, 0.0f), 0, Vector2(0, 0));
	EndUI();
}

void Game::BeginUI()
{
	// Sprites don't use a shader 
	m_renderer->SetCurrentShader(NULL);

	CommonStates states(m_renderer->GetDevice());
	m_spriteBatch->Begin(SpriteSortMode_Deferred, states.NonPremultiplied());
}

void Game::EndUI()
{
	m_spriteBatch->End();
}

void Game::Shutdown()
{
	for (unsigned int i = 0; i < m_gameObjects.size(); i++)
	{
		delete m_gameObjects[i];
	}

	m_gameObjects.clear();

	if (m_currentCam)
	{
		delete m_currentCam;
		m_currentCam = NULL;
	}

	if (m_unlitVertexColouredShader)
	{
		m_unlitVertexColouredShader->Release();
		delete m_unlitVertexColouredShader;
		m_unlitVertexColouredShader = NULL;
	}

	if (m_unlitTexturedShader)
	{
		m_unlitTexturedShader->Release();
		delete m_unlitTexturedShader;
		m_unlitTexturedShader = NULL;
	}

	if(m_diffuseTexturedShader)
	{
		m_diffuseTexturedShader->Release();
		delete m_diffuseTexturedShader;
		m_diffuseTexturedShader = NULL;
	}

	if (m_diffuseTexturedFogShader)
	{
		m_diffuseTexturedFogShader->Release();
		delete m_diffuseTexturedFogShader;
		m_diffuseTexturedFogShader = NULL;
	}

	if (m_meshManager)
	{
		m_meshManager->Release();
		delete m_meshManager;
		m_meshManager = NULL;
	}

	if (m_textureManager)
	{
		m_textureManager->Release();
		delete m_textureManager;
		m_textureManager = NULL;
	}

	if (m_stateMachine)
	{
		delete m_stateMachine;
		m_stateMachine = NULL;
	}

	if (m_spriteBatch)
	{
		delete m_spriteBatch;
		m_spriteBatch = NULL;
	}

	if (m_arialFont12)
	{
		delete m_arialFont12;
		m_arialFont12 = NULL;
	}

	if (m_arialFont18)
	{
		delete m_arialFont18;
		m_arialFont18 = NULL;
	}

	if (m_sceneLighting)
	{
		delete m_sceneLighting;
		m_sceneLighting = NULL;
	}

	if (m_audio)
	{
		m_audio->Shutdown();
		delete m_audio;
		m_audio = NULL;
	}
}